import useSWR from 'swr';

const useAuth = () => {
    const {data, error} = useSWR('/api/auth/user');
    console.log('use auth data: ', data?.data);

    return {
        data: data?.data,
        error,
        isLoading: !data && !error
    };
}

export default useAuth;
