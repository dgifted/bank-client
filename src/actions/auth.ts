import { updateAPIConfig } from 'api/base';
import { loginUser, registerUser } from 'api/auth';
import * as actionTypes from './actionTypes';

import { AppDispatch } from 'store';

// Tools
import { setAuthToken } from 'tools';

export const setAuthStatus = (status: boolean, email: string = '') => ({
   type: actionTypes.SET_AUTH_STATUS,
   status,
   email,
});

// type UserData = {
//    jwt: string;
// };

type IUserData = {
   token: string;
   detail: {
      email: string,
      id: number
   };
};

// Login
export const login = (data: {}) => async (dispatch: AppDispatch) => {
   try {
      const userData: IUserData = await loginUser(data);

      if (!userData) {
         dispatch(setAuthStatus(false));
         return;
      }

      setAuthToken(userData.token);
      updateAPIConfig({ authToken: userData.token });
      dispatch(setAuthStatus(true, userData.detail.email));

      return Promise.resolve(userData);
   } catch (err) {
      dispatch(setAuthStatus(false));

      return Promise.reject(err)
   }
};

// Register
export const register = (data: {}) => async (dispatch: AppDispatch) => {
   try {
      const user = await registerUser(data);

      if (!user) {
         dispatch(setAuthStatus(false));
         return;
      }

      dispatch(setAuthStatus(true));
   } catch (err) {
      dispatch(setAuthStatus(false));
   }
};
