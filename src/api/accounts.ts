import { callAPI } from './base';

// Get user's accounts
export const getMyAccounts = () => callAPI(`/api/account/user-accounts`);

// Get single account
export const getSingleAccount = (id: string) => callAPI(`/api/account/user-single-account/${id}`); 
