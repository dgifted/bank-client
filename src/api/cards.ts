import { callAPI } from './base';

// Get user's cards
export const getMyCards = () => callAPI(`/api/card/user-cards`);

// Get single card
export const getSingleCard = (id: string) => callAPI(`/api/card/user-single-card/${id}`);

// Change card's PIN
export const changePin = (id: string, data: {}) => callAPI(`/api/card/${id}/change-pin`, 'post', data);

// Change card's limits
export const changeLimits = (id: string, data: {}) =>
   callAPI(`/cards/${id}/change-limits`, 'put', data);
