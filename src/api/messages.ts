import { callAPI } from './base';

// Get user's messages
export const getMyMessages = () => callAPI(`/api/message/user-unread-messages`);

// Get single message
export const getSingleMessage = (id: string) => callAPI(`/messages/${id}`);

// Toggle message read
export const toggleMessageRead = (id: string) => callAPI(`/messages/${id}/toggle-read`, 'post');

// Remove message
export const removeMessage = (id: string) => callAPI(`/api/notification/${id}/delete`, 'delete');
