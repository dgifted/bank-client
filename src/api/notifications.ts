import {callAPI} from './base';

export const getMyNotifications = () => callAPI(`/api/notification/user-notifications`);

export const getSingleNotification = (id: string) => callAPI(`/api/notification/user-single-notification/${id}`);

export const removeNotification = (id: string) => callAPI(`/api/notification/${id}/delete`);
