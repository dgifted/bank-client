import { callAPI } from './base';

// Get user's transfers
export const getMyTransfers = () => callAPI(`/api/transfer/user-transfers`);

// Get single transfer
export const getSingleTransfer = (id: string) => callAPI(`/api/transfer/user-single-transfer${id}`);

// Create a new transfer
export const createTransfer = (data: {}) => callAPI(`/api/transfer/create`, 'post', data);
