import { callAPI } from './base';

// Get myself
export const getMyself = () => callAPI(`/api/auth/user`);

// Update myself
export const updateMyself = (data: {}) => callAPI(`/users/me`, 'put', data);
