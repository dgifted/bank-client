import React from 'react';
import {Link} from 'react-router-dom';
import {chunker} from 'tools';

type Props = {
    matchUrl: string;
    id: string;
    type: string;
    sortcode: number;
    currency: string;
    balance: number;
    accountTypeDesc: string;
    isActive: number;
};

const AccountsListEl: React.FC<Props> = (props) => {
    const {matchUrl, id, type, sortcode, currency, balance, accountTypeDesc, isActive} = props;

    return (
        <Link to={`${matchUrl}/${id}`} className="list-group-item list-group-item-action">
            <div className="d-flex w-100 justify-content-between">
                <h5 className="mb-1 text-capitalize">{type} account</h5>
                <small className="text-muted">{isActive === 1 ? 'Active' : 'Inactive'}</small>
            </div>
            {/*<p className="mb-1">*/}
            {/*    {accountTypeDesc}*/}
            {/*</p>*/}
            <small className="text-muted">
                Sortcode: {chunker(sortcode, 2, '-')}, currency: {currency}, balance: <i className="text-uppercase">{currency}</i> {balance}

            </small>
        </Link>
    );
};

export default AccountsListEl;
