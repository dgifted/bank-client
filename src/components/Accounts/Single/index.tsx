import React, {useEffect, useState} from 'react';
import {RouteComponentProps, useHistory} from 'react-router-dom';

import {useAppSelector} from '@hooks';
import {chunker} from 'tools';
import {FiArrowLeft} from "@react-icons/all-files/fi/FiArrowLeft";

interface Params {
    accId: string;
}

interface Props extends RouteComponentProps<Params> {
}

const SingleAccount: React.FC<Props> = (props) => {
    const accId = props.match.params.accId;
    const singleAcc = useAppSelector((state) => state.accounts.data.find((el) => +el.id === +accId));
    const [account, setAccount] = useState<any | null>();
    const routerHistory = useHistory();

    useEffect(() => {
        const acc = {
            type: singleAcc.type,
            balance: singleAcc.balance,
            currency: singleAcc.currency.abbreviation,
            number: singleAcc.number,
            sortcode: singleAcc.sort_code
        };
        setAccount(acc);
    }, [setAccount]);

    console.log('The account -> ', account);
    return (
        <section className="module single-account">
            <div className={'d-flex justify-content-start justify-content-md-end'}>
                <button
                    className={'btn btn-outline-primary btn-sm'}
                    onClick={() => routerHistory.push(`/panel/accounts`)}
                >
                    <FiArrowLeft/>
                    Back to accounts list
                </button>
            </div>

            <h1 className={'text-capitalize'}>{account?.type} account</h1>
            <ul>
                <li>Sortcode: {chunker(account?.sortcode, 2, '-')}</li>
                <li>Number: {account?.number}</li>
                <li>Currency: {account?.currency}</li>
                <li className="text-uppercase">
                    Balance: <span className="">{account?.currency}</span> {account?.balance}
                </li>
            </ul>
        </section>
    );
};

export default SingleAccount;
