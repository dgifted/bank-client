import React, {useEffect} from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import {useAppDispatch} from "@hooks";
import * as actions from "actions";
import {isValidToken} from "tools";

import Layout from "containers/Layout";

import CurrencyStats from "containers/CurrencyStats";
import Panel from "containers/Panel";

import Login from "containers/Auth/Login";
import Register from "containers/Auth/Register";
import Logout from "components/Auth/Logout";
import PageNotFound from "components/PageNotFound";
import 'react-loading-skeleton/dist/skeleton.css';
import "./app.scss";
import MoveToTop from "components/Misc/MoveToTop";

const App: React.FC = () => {
    const dispatch = useAppDispatch();

    const setAuthStatus = (status: boolean) =>
        dispatch(actions.setAuthStatus(status));

    useEffect(() => {
        isValidToken()
            .then(() => {
                setAuthStatus(true);
            })
            .catch(() => {
                setAuthStatus(false);
            });
    }, []);

    return (
      <Layout>
        <Switch>
          <Route path="/panel" component={Panel} />
          <Route path="/currencies" component={CurrencyStats} />
          <Route path="/login" component={Login} />
          <Route path="/logout" component={Logout} />
          <Route path="/register" component={Register} />
          <Route exact path="/" >
              <Redirect to={'/panel'}/>
          </Route>
          <Route component={PageNotFound} />
        </Switch>
        <MoveToTop />
      </Layout>
    );
};

export default App;
