import React, { useEffect } from "react";
import {useHistory} from 'react-router-dom';
import SmallFormBox from "components/UI/FormBoxes/Small";
import LoginForm from "./Form";
import Loader from "components/UI/Loader";

import * as H from "history";
import { getAuthToken } from "tools";

type Props = {
  history: H.History;
  onLoginSubmit: (identifier: string, password: string) => void;
  loading: boolean;
  error: string;
};

const LoginBox: React.FC<Props> = (props) => {
   const routerHistory = useHistory();

  useEffect(() => {
     const token = getAuthToken();
     if (token){
      routerHistory.replace('/panel');
     }
  }, []);

  return (
    <SmallFormBox>
      {props.error}
      {props.loading ? (
        <Loader />
      ) : (
        // @ts-ignore
        <LoginForm
          history={props.history}
          onLoginSubmit={props.onLoginSubmit}
        />
      )}
    </SmallFormBox>
  );
};

export default LoginBox;
