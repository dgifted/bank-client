import React from "react";
import { Field, Form, withFormik, FormikProps } from "formik";
import { Link } from "react-router-dom";
import * as Yup from "yup";
import SingleModuleButton from "components/UI/Buttons/SingleModuleButton";

import * as H from "history";
import { useEffect } from "react";
import { useState } from "react";
import { callAPI } from "api/base";
import axios from "axios";
// import { log } from "console";

// Shape of form values
interface FormValues {
  email: string;
  password: string;
  passwordConfirm: string;
  firstName: string;
  lastName: string;
  phone: string;
  dob: any;
  streetAddress: string;
  postCode: string;
  city: string;
  sex: string;
  country: string;
  occupation: string;
  accountType: string;
  currency: string;
  passport?: File | undefined;
}

const InnerForm = (props: FormikProps<FormValues>) => {
  const { errors, touched } = props;
  const [accountTypes, setAccountTypes] = useState<any[]>([]);
  const [countries, setCountries] = useState<any[]>([]);
  const [currencies, setCurrencies] = useState<any[]>([]);

  const initPageData = async () => {
    try {
      const [res1, res2, res3] = await axios.all([
        callAPI(`/api/countries`),
        callAPI(`/api/currencies`),
        callAPI(`/api/account-types`),
      ]);

      setCountries(res1);
      setCurrencies(res2);
      setAccountTypes(res3);
    } catch (err) {
      setCountries([]);
    }
  };

  useEffect(() => {
    initPageData().then();
  }, []);

  return (
    <Form className="register-form">
      <div>
        {/* <p>
               <b>Registration is currently inactive</b>
            </p> */}
        <div className="form-group">
          <Field
            type="text"
            className="form-control login-input"
            name="firstName"
            placeholder="Your first name..."
          />
          {touched.firstName && errors.firstName && (
            <p className="field-invalid">{errors.firstName}</p>
          )}
        </div>
        <div className="form-group">
          <Field
            type="text"
            className="form-control login-input"
            name="lastName"
            placeholder="Your last name..."
          />
          {touched.lastName && errors.lastName && (
            <p className="field-invalid">{errors.lastName}</p>
          )}
        </div>
        <div className="form-group">
          <Field
            type="email"
            className="form-control "
            name="email"
            placeholder="Your email..."
          />
          {touched.email && errors.email && (
            <p className="field-invalid">{errors.email}</p>
          )}
        </div>
        <div className="form-group">
          <Field
            type="password"
            className="form-control "
            name="password"
            placeholder="Your password..."
          />
          {touched.password && errors.password && (
            <p className="field-invalid">{errors.password}</p>
          )}
        </div>
        <div className="form-group">
          <Field
            type="password"
            className="form-control "
            name="passwordConfirm"
            placeholder="Confirm password..."
          />
          {touched.passwordConfirm && errors.passwordConfirm && (
            <p className="field-invalid">{errors.password}</p>
          )}
        </div>
        <div className="form-group">
          <Field
            type="date"
            className="form-control "
            name="dob"
            placeholder="Your date of birth..."
          />
          {touched.dob && errors.dob && (
            <p className="field-invalid">{errors.dob}</p>
          )}
        </div>
        <div className="form-group">
          <Field
            type="tel"
            className="form-control "
            name="phone"
            placeholder="Your mobile number..."
          />
          {touched.phone && errors.phone && (
            <p className="field-invalid">{errors.phone}</p>
          )}
        </div>
        <div className="form-group">
          <Field as="select" className="form-control " name="country">
            <option>Choose country</option>
            {countries && countries.length > 0 && (
              <>
                {countries.map((country: any) => (
                  <option
                    key={country.id.toString()}
                    value={country.id + ":" + country.name}
                  >
                    {country.name}
                  </option>
                ))}
              </>
            )}
          </Field>
          {touched.country && errors.country && (
            <p className="field-invalid">{errors.country}</p>
          )}
        </div>
        <div className="form-group">
          <Field
            type="text"
            className="form-control "
            name="city"
            placeholder="Your city address..."
          />
          {touched.city && errors.city && (
            <p className="field-invalid">{errors.city}</p>
          )}
        </div>
        <div className="form-group">
          <Field
            type="text"
            className="form-control "
            name="streetAddress"
            placeholder="Your street address..."
          />
          {touched.streetAddress && errors.streetAddress && (
            <p className="field-invalid">{errors.streetAddress}</p>
          )}
        </div>
        <div className="form-group">
          <Field
            type="text"
            className="form-control "
            name="postCode"
            placeholder="Your postal code..."
          />
          {touched.postCode && errors.postCode && (
            <p className="field-invalid">{errors.postCode}</p>
          )}
        </div>

        <div className="form-group">
          <Field as="select" className="form-control" name="sex">
            <option>Choose your gender</option>
            <option value="female">Female</option>
            <option value="male">Male</option>
            <option value="others">Others</option>
          </Field>
          {touched.sex && errors.sex && (
            <p className="field-invalid">{errors.sex}</p>
          )}
        </div>
        <div className="form-group">
          <Field as="select" className="form-control" name="accountType">
            <option>Choose account type</option>
            {accountTypes && accountTypes.length > 0 && (
              <>
                {accountTypes.map((type: any) => (
                  <option
                    key={type.id.toString()}
                    value={type.id + ":" + type.name}
                  >
                    {type.title}
                  </option>
                ))}
              </>
            )}
          </Field>
          {touched.accountType && errors.accountType && (
            <p className="field-invalid">{errors.accountType}</p>
          )}
        </div>
        <div className="form-group">
          <Field as="select" className="form-control" name="currency">
            <option>Choose currency</option>
            {currencies && currencies.length > 0 && (
              <>
                {currencies.map((currency: any) => (
                  <option
                    key={currency.id.toString()}
                    value={currency.id + ":" + currency.name}
                  >
                    {currency.name} - {currency.abbreviation.toUpperCase()}
                  </option>
                ))}
              </>
            )}
          </Field>
          {touched.currency && errors.currency && (
            <p className="field-invalid">{errors.currency}</p>
          )}
        </div>
        <div className="form-group">
          <Field
            type="text"
            className="form-control "
            name="occupation"
            placeholder="Your occupation..."
          />
          {touched.occupation && errors.occupation && (
            <p className="field-invalid">{errors.occupation}</p>
          )}
        </div>
        {/* <div className="form-group">
          <Field
            type="file"
            accept="image/*"
            className="form-control "
            name="passport"
          />
          {touched.passport && errors.passport && (
            <p className="field-invalid">{errors.passport}</p>
          )}
        </div> */}
        <p>
          <Link to="/login">Already have an account?</Link>
        </p>
      </div>

      <SingleModuleButton text="Create your account" type="submit" />
    </Form>
  );
};

// The type of props MyForm receives
interface MyFormProps extends FormValues {
  history: H.History;
  //   email: string;
  //   password: string;
  onRegisterSubmit: (
    email: string,
    password: string,
    passwordConfirm: string,
    firstName: string,
    lastName: string,
    phone: string,
    dob: any,
    streetAddress: string,
    postCode: string,
    city: string,
    sex: string,
    country: string,
    occupation: string,
    accountType: string,
    currency: string,
    passport?: File | undefined
  ) => void;
}

// Wrap our form with the using withFormik HoC
const RegisterForm = withFormik<MyFormProps, FormValues>({
  // Transform outer props into form values
  mapPropsToValues: (props) => ({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    passwordConfirm: "",
    phone: "",
    dob: "",
    streetAddress: "",
    postCode: "",
    city: "",
    sex: "",
    country: "",
    occupation: "",
    accountType: "",
    currency: "",
    passport: undefined,
  }),

  // Add a custom validation function (this can be async too!)
  validationSchema: Yup.object().shape({
    email: Yup.string()
      .required("Email is required")
      .email("This is not a valid email"),
    password: Yup.string()
      .required("Password is required")
      .min(6, "Your password has to be at least 6 characters"),
    passwordConfirm: Yup.string().when("password", {
      is: (val: any) => (val && val.length > 0 ? true : false),
      then: Yup.string().oneOf(
        [Yup.ref("password")],
        "Both password need to be the same"
      ),
    }),
    firstName: Yup.string().required("First name is required"),
    lastName: Yup.string().required("Last name is required"),
    phone: Yup.string().required("Phone name is required"),
    dob: Yup.date().required("Date of birth name is required"),
    streetAddress: Yup.string().required("Street address is required"),
    postCode: Yup.string().required("Postal code is required"),
    city: Yup.string().required("City is required"),
    sex: Yup.string().required("Gender is required"),
    country: Yup.string().required("Country is required"),
    accountType: Yup.string().required("Account type is required"),
    currency: Yup.string().required("Currency is required"),
    occupation: Yup.string().required("Occupation is required"),
    //  passport: Yup.().required("Passport address is required"),
  }),

  // Submission handler
  handleSubmit: (values, { props }) => {
    props.onRegisterSubmit(
      values.email,
      values.password, 
      values.passwordConfirm,
      values.firstName,
      values.lastName,
      values.phone, 
      values.dob,
      values.streetAddress,
      values.postCode,
      values.city,
      values.sex,
      values.country,
      values.occupation,
      values.accountType,
      values.currency,
      values.passport
    );
  },
})(InnerForm);

export default RegisterForm;
