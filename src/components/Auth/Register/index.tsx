import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import SmallFormBox from "components/UI/FormBoxes/Small";
import RegisterForm from "./Form";
import Loader from "components/UI/Loader";

import * as H from "history";
import { getAuthToken } from "tools";

type Props = {
  history: H.History;
  onRegisterSubmit: (
    email: string,
    password: string,
    passwordConfirm: string,
    firstName: string,
    lastName: string,
    phone: string,
    dob: any,
    streetAddress: string,
    postCode: string,
    city: string,
    sex: string,
    country: string,
    occupation: string,
    accountType: string,
    currency: string,
    passport?: File | undefined
  ) => void;
  loading: boolean;
  error?: string;
};


const RegisterBox: React.FC<Props> = (props) => {

   const routerHistory = useHistory();

   useEffect(() => {
     const token = getAuthToken();
     if (token) {
       routerHistory.replace("/panel");
     }
   }, []);

  return (
    <SmallFormBox>
      {props.error && (
        <p className="text-center text-danger">{props.error}</p>
      )}
      {props.loading ? (
        <Loader />
      ) : (
        // @ts-ignore
        <RegisterForm
          history={props.history}
          onRegisterSubmit={props.onRegisterSubmit}
        />
      )}
    </SmallFormBox>
  );
};

export default RegisterBox;
