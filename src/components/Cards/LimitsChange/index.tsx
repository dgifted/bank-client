import React from 'react';
import { useAppSelector, useAppDispatch } from '@hooks';

import * as actions from 'actions';
import { chunker } from 'tools';
import Form from './Form';

import { RouteComponentProps } from 'react-router-dom';

interface Params {
   cardId: string;
}

interface Props extends RouteComponentProps<Params> {}

const LimitsChange: React.FC<Props> = (props) => {
   const dispatch = useAppDispatch();

   const cardId = props.match.params.cardId;

   const singleCard = useAppSelector((state) => state.cards.data.find((el) => +el.id === +cardId));
   const card = {
      type: singleCard.type_name,
      number: singleCard.number,
      dailyOnlineLimit: '',
      dailyWithdrawalLimit: ''
   };

   const currentOnlineLimit = card.dailyOnlineLimit || 'unlimited';
   const currentWithdrawalLimit = card.dailyWithdrawalLimit || 'unlimited';

   const changeCardLimits = (newOnlineLimit: string, newWithdrawalLimit: string) =>
      dispatch(actions.changeCardLimits(cardId, newOnlineLimit, newWithdrawalLimit));

   return (
      <div className="col-sm-6 offset-sm-3">
         <section className="module limits-change">
            <h1>Limits change</h1>
            <p>
               <strong>{card.type} card</strong>
            </p>
            <p>Number: {chunker(card.number, 4, '-')}</p>
            {/*@ts-ignore*/}
            <Form
               changeCardLimits={changeCardLimits}
               currentOnlineLimit={currentOnlineLimit}
               currentWithdrawalLimit={currentWithdrawalLimit}
            />
         </section>
      </div>
   );
};

export default LimitsChange;
