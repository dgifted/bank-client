import React from 'react';
import { useAppSelector } from '@hooks';

import CardsListEl from '../ListElement';

import { RouteComponentProps } from 'react-router-dom';

interface Props extends RouteComponentProps {}

type CardType = {
   _id: string;
};

const CardsList: React.FC<Props> = (props) => {
   const { match } = props;
   const cards = useAppSelector((state) => state.cards.data);

   const cardsList = cards.map(item => ({
       ...item,
       type: item.type_name,
       expiresMonth: item.expires_month,
       expiresYear: item.expires_year,
       dailyOnlineLimit: item.daily_online_limit,
       dailyWithdrawalLimit: item.daily_withdrawal_limit,
       monthlyOnlineLimit: item.monthly_online_limit,
       monthlyWithdrawalLimit: item.monthly_withdrawal_limit,
   }))
       .filter(item => +item.is_active === 1)
       .map((card: CardType) => (
       // @ts-ignore
       <CardsListEl key={card.id} {...card} matchUrl={match.url} />
   ));

   return (
      <>
         <h1>Cards</h1>

         <p>You have {cardsList.length} active cards</p>
         <div className="list-group">{cardsList}</div>
      </>
   );
};

export default CardsList;
