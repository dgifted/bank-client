import React from 'react';
import { useAppSelector } from '@hooks';

import CardInfobox from 'components/Infobox/CardInfobox';

import { RouteComponentProps } from 'react-router-dom';

interface Params {
   cardId: string;
}

interface Props extends RouteComponentProps<Params> {}

const SingleCard: React.FC<Props> = (props) => {
   const { match } = props;

   const cardId = props.match.params.cardId;
   const singleCard = useAppSelector((state) => state.cards.data.find((el) => +el.id === +cardId));
   const card = {
       ...singleCard,
       expiresMonth: singleCard.expires_month,
       expiresYear: singleCard.expires_year,
       income7Days: 3000,
       expenses7Days: 1500,
   };

   return (
      <div className="row">
         <div className="col">
            <CardInfobox {...card} currentUrl={match.url} />
         </div>
      </div>
   );
};

export default SingleCard;
