import React, {useEffect, useState} from 'react';
import {RouteComponentProps} from 'react-router-dom';
import {useAppDispatch, useAppSelector} from '@hooks';

import * as actions from 'actions';
import MessagesListEl from '../ListElement';
import {callAPI} from "api/base";

interface Props extends RouteComponentProps {
    messages: {
        id: string;
        title: string;
        isRead: boolean;
        sentDate: Date;
    }[];
}

const MessagesList: React.FC<Props | undefined> = (props) => {
    const dispatch = useAppDispatch();
    const [userMessages, setUserMessages] = useState<any[]>([]);

    const [search, setSearch] = useState('');

    // const messages = useAppSelector((state) => state.messages.data);

    const messageToggle = (id: string) => dispatch(actions.messageToggle(id));

    const messageRemove = (id: string) => {
        dispatch(actions.messageRemove(id)).then();
    }
    const findMessage = (value: string) => setSearch(value);

    const removeMessage = async (id: string) => {
        try {
            await callAPI(`/api/notification/${id}/delete`);
            setUserMessages(userMessages.filter(msg => +msg.id !== +id));
        } catch (err) {

        }
    };

    const fetchUserMessages = async () => {
        try {
            const resp = await callAPI(`/api/notification/user-notifications`);
            setUserMessages(resp.data.data.map((item: any) => ({
                ...item,
                title: item.description,
                isRead: 0,
                sentDate: new Date(item.createdAt)
            })));
        } catch (err) {
            console.log('Error fetching messages');
            setUserMessages([]);
        }
    };

    // Messages
    // Allow search by message title
    const searchText = search.toLowerCase();
    // const messagesList = messages
    const messagesList = userMessages
        .filter((message) => message.title.toLowerCase().includes(searchText))
        .map(item => ({
            ...item,
            //@ts-ignore
            isRead: Number(item.is_read) === 1,
            //@ts-ignore
            sentDate: item.created_at
        }))
        .map((message) => (
            <MessagesListEl
                {...message}
                key={message.id}
                matchUrl={props.match.url}
                onToggle={() => messageToggle(message.id)}
                onRemove={() => messageRemove(message.id)}
            />
        ));

    useEffect(() => {
        fetchUserMessages().then(r => r);
    }, []);

    return (
        <>
            <h1>Messages</h1>

            {/*<p>There are {messages.length} messages in your box</p>*/}
            <p>There have {userMessages.length} notification messages.</p>

            <div className="form-group">
                <input
                    className="form-control"
                    placeholder="Search for..."
                    onChange={(e) => findMessage(e.target.value)}
                />
            </div>

            <div className="list-group">{messagesList}</div>
        </>
    );
};

export default MessagesList;
