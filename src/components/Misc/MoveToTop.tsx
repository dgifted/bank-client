import React, {useLayoutEffect} from "react";
import {useLocation} from 'react-router-dom';

const MoveToTop: React.FC = () => {
    const routePath = useLocation();

    const toTop = () => window.scrollTo({
        top: 0,
        left: 0,
    })

    useLayoutEffect(() => {
        toTop()
    }, [routePath]);

    return null;
};

export default MoveToTop;
