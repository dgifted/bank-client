import React from 'react';
import {Link} from 'react-router-dom';

import './style.scss';
import useAuth from "../../../@hooks/use-auth";


type Props = {
    user: {
        firstName: string;
        lastName: string;
        passport: string;
        sex: string;
    };
    toggleMobileNav: () => void;
};

const NavigationHeader: React.FC<Props> = (props) => {
    const {data: profileData} = useAuth();

    return (
        <header className="navigation-header">
            <div className="user-profile-box">
                <Link to="/panel/profile">
                    {/*@ts-ignore*/}
                    <img src={!!profileData?.avatar ? profileData?.avatar : ''} alt="User profile" height="50" width="50"/>
                    <span>
                  {props.user.firstName} {props.user.lastName}
               </span>
                </Link>
            </div>

            <ul className="navigation-header-links">
                <li>
                    <Link to="/logout">Logout</Link>
                </li>
                <li className="toggle-menu">
                    <button onClick={props.toggleMobileNav}>
                        <i className="ion-navicon-round"/>
                    </button>
                </li>
            </ul>
        </header>
    );
};

export default NavigationHeader;
