import React from "react";
import {useHistory} from 'react-router-dom'
import {createBrowserHistory} from 'history';
import {FiArrowLeft} from "@react-icons/all-files/fi/FiArrowLeft";
import {BiWalletAlt} from "@react-icons/all-files/bi/BiWalletAlt";


const SubHeaderNav: React.FC = () => {
    const hist = createBrowserHistory();
    const routerHistory = useHistory();

    return (
        <div className={'d-flex align-items-center justify-content-start justify-content-md-between px-2'}>
            <button
                className={'btn btn-outline-primary btn-sm'}
                onClick={() => hist.back()}
            >
                <FiArrowLeft />{' '}Back
            </button>

            {/*<button*/}
            {/*    className={'btn btn-primary btn-sm'}*/}
            {/*    onClick={() => routerHistory.push('/panel/deposit')}*/}
            {/*>*/}
            {/*    <BiWalletAlt />{' '}Fund Account*/}
            {/*</button>*/}
        </div>
    );
};

export default SubHeaderNav;
