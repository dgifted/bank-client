import React from 'react';
import Skeleton from 'react-loading-skeleton';

import { formatDate } from 'tools';
import './style.scss';

type Props = {
   profile: {
      createdAt: Date;
      firstName: string;
      lastName: string;
      streetAddr: string;
      postcode: string;
      city: string;
      email: string;
      avatar: string
   };
};

const ProfileHeader: React.FC<Props | undefined> = (props) => {
   const {profile} = props;

   // let createdAt = formatDate(props?.profile?.createdAt, 'dd/MM/yyyy HH:mm');

   return (
      <header className="profile-header">
         <div className="profile-user-info">
            <h3>
               {/*{profile?.firstName || 'N/A'} {profile?.lastName ?? 'N/A'}*/}
               {profile?.firstName || <Skeleton/>} {profile?.lastName ?? <Skeleton/>}
            </h3>
            <p>
               {/*{profile?.streetAddr || 'N/A'}, {profile?.postcode ?? 'N/A'} {profile?.city || 'N/A'}*/}
               {profile?.streetAddr || <Skeleton/>}, {profile?.postcode ?? <Skeleton/>} {profile?.city || <Skeleton/>}
            </p>
            <p>
               {/*<strong>Email: </strong> {profile?.email || 'N/A'}*/}
               <strong>Email: </strong> {profile?.email || <Skeleton/>}
            </p>
            <p>
               {/*<strong>Registered on</strong> {createdAt || 'N/A'}*/}
            </p>
         </div>

         {/*<img src={profile?.avatar} alt={`${profile?.firstName || 'N/A'} ${profile?.lastName || 'N/A'} profile`} />*/}
         {profile?.avatar ? <img src={profile?.avatar} alt={`${profile?.firstName || 'N/A'} ${profile?.lastName || 'N/A'} profile`} /> : <Skeleton/>}

      </header>
   );
};

export default ProfileHeader;
