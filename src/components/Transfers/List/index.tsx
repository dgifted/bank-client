import React, {useEffect, useRef, useState} from 'react';
import {Link, RouteComponentProps} from 'react-router-dom';
import {useAppSelector} from '@hooks';

import TransfersListEl from '../ListElement';

const refreshTransfers = (
    newVal: string,
    setSearch: React.Dispatch<React.SetStateAction<string>>
) => {
    setSearch(newVal);
};

interface Props extends RouteComponentProps {
}

const TransfersList: React.FC<Props> = (props) => {
    const [search, setSearch] = useState('');
    const {match} = props;

    const searchRef = useRef<HTMLInputElement>(null);
    const transfers = useAppSelector((state) => state.transfers.data);
    const account = useAppSelector((state) => state.accounts.data)[0];

    // Allow filtering by payee's name or transfer reference
    const searchText = search.toLowerCase();
    const transfersList = transfers
        .map(item => ({
            id: item.id,
            currency: item.currency.abbreviation,
            payeeName: item.payee_name,
            date: item.created_at,
            amount: item.amount,
            status: item.status_name,
            reference: item.ref_id,
            type: item.type_name
        }))
        .filter(
            (t) =>
                t.payeeName.toLowerCase().includes(searchText) ||
                t.reference.toLowerCase().includes(searchText)
        )
        .map((t) => <TransfersListEl key={t.id} {...t} matchUrl={props.match.url}/>);

    useEffect(() => {

    }, []);

    return (
        <div>
            <h1>Transfers</h1>

            <p>There are {transfers.length} transfers right now!</p>
            <p>
                {account.status && +account.status === 1 && (
                    <Link to={`${match.url}/new`} className="btn btn-primary">
                        New transfer
                    </Link>
                )}
                {account.status && +account.status !== 1 && (
                    <span>You cannot make transfers from a {account.status_name} account. Contact us info@sandtrustonlinebank.com.</span>
                )}
            </p>

            <div className="form-group">
                <input
                    className="form-control"
                    placeholder="Search for (payee/reference)..."
                    onChange={() => {
                        const val = searchRef.current?.value;

                        if (val) {
                            refreshTransfers(val, setSearch);
                        }
                    }}
                    ref={searchRef}
                />
            </div>

            <div className="list-group">{transfersList}</div>
        </div>
    );
};

export default TransfersList;
