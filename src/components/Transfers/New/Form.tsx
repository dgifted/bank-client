import React from 'react';
import {Field, Form, FormikProps, withFormik} from 'formik';
import SingleModuleButton from 'components/UI/Buttons/SingleModuleButton';
import validations from './validations';

interface FormValues {
    sourceAccountId: string;
    payeeAccountNumber: string;
    payeeSortcode: string;
    payeeName: string;
    payeeBankName: string;
    amount: string;
    userAccountsList: any;
}

const InnerForm = (props: FormikProps<FormValues>) => {
    const {errors, touched} = props;

    return (
        <Form>
            <div>
                <div className="form-group">
                    <label htmlFor="source-account">Choose your account</label>
                    <Field
                        as="select"
                        className="form-control"
                        id="source-account"
                        name="sourceAccountId"
                        readonly={true}
                    >
                        {/*@ts-ignore*/}
                        {props.userAccountsList.slice(0, 1).map((item:any) => (
                            <option value={item.id} key={item.id.toString()}>
                                {`${item.type}, ${item.sortcode}, ${item.balance} ${item.currency}`}
                            </option>
                        ))}
                    </Field>
                    {touched.sourceAccountId && errors.sourceAccountId && (
                        <p>{errors.sourceAccountId}</p>
                    )}
                </div>

                <div className="form-group">
                    <label htmlFor="payee-acc-number">Recipient's account number</label>

                    <Field
                        type="number"
                        className="form-control"
                        id="payee-acc-number"
                        name="payeeAccountNumber"
                        maxLength="8"
                        placeholder="Recipient's account number..."
                    />
                    {touched.payeeAccountNumber && errors.payeeAccountNumber && (
                        <p className="field-invalid">{errors.payeeAccountNumber}</p>
                    )}
                </div>

                <div className="form-group">
                    <label htmlFor="payee-sort-code">Recipient's sort code</label>

                    <Field
                        type="number"
                        className="form-control"
                        id="payee-sort-code"
                        name="payeeSortcode"
                        maxLength="6"
                        placeholder="Recipient's sort code..."
                    />
                    {touched.payeeSortcode && errors.payeeSortcode && (
                        <p className="field-invalid">{errors.payeeSortcode}</p>
                    )}
                </div>

                <div className="form-group">
                    <label htmlFor="payee-name">Recipient's name</label>

                    <Field
                        type="text"
                        className="form-control"
                        id="payee-name"
                        name="payeeName"
                        placeholder="Recipient's name..."
                    />
                    {touched.payeeName && errors.payeeName && (
                        <p className="field-invalid">{errors.payeeName}</p>
                    )}
                </div>

                <div className="form-group">
                    <label htmlFor="payee-name">Recipient's bank name</label>

                    <Field
                        type="text"
                        className="form-control"
                        id="payee-bank-name"
                        name="payeeBankName"
                        placeholder="Recipient's bank name..."
                    />
                    {touched.payeeBankName && errors.payeeBankName && (
                        <p className="field-invalid">{errors.payeeBankName}</p>
                    )}
                </div>

                <div className="form-group">
                    <label htmlFor="amount">Amount</label>

                    <Field
                        type="number"
                        className="form-control"
                        id="amount"
                        name="amount"
                        placeholder="Amount..."
                    />
                    {touched.amount && errors.amount && <p className="field-invalid">{errors.amount}</p>}
                </div>

                <p className="validation-info">{props.status}</p>
            </div>

            <SingleModuleButton text="Continue" type="submit"/>
        </Form>
    );
};

interface MyFormProps extends FormValues {
    userId: string;
    firstAccId: string;
    addTransfer: (data: {}) => Promise<any>;
}

// Wrap our form with the using withFormik HoC
const NewTransferForm = withFormik<MyFormProps, FormValues>({
    // Transform outer props into form values
    mapPropsToValues: (props) => ({
        sourceAccountId: props.firstAccId,
        payeeAccountNumber: '',
        payeeSortcode: '',
        payeeName: '',
        payeeBankName: '',
        amount: '',
        userAccountsList: props.userAccountsList,
    }),

    validationSchema: validations,

    // Submission handler
    handleSubmit: (values, {props}) => {
        // Prepare some data for API
        const data = {
            ...values,
            sender: props.userId,
            recipient: props.userId,
        };

        props
            .addTransfer(data)
            .then((data) => {})
            .catch((error) => {});
    },
})(InnerForm);

export default NewTransferForm;
