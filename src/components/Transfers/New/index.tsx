import React, {useEffect, useRef, useState} from 'react';
import {useAppDispatch, useAppSelector} from '@hooks';
import Form from './Form';
import {callAPI} from "api/base";
import SingleModuleButton from "components/UI/Buttons/SingleModuleButton";
import Loader from "components/UI/Loader";
import * as actions from 'actions';

type Props = {};

const NewTransfer: React.FC<Props> = (props) => {
    const dispatch = useAppDispatch();
    const [transferInitiated, setTransferInitiated] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [otpValue, setOtpValue] = useState<string>('');
    const [error, setError] = useState<any>();
    const [transferCompleted, setTransferCompleted] = useState(false);

    const accounts = useAppSelector((state) => state.accounts.data);
    const userId = useAppSelector((state) => state.profile.data.id);

    // const addTransfer1 = (data: any) => dispatch(actions.addTransfer(data));
    const addTransfer = async (data: any) => {

        const formData = {
            ...data,
            currencyId: accounts[0].currency_id
        }
        // console.log('formData -> ', formData); return;
        setIsLoading(true);

        try {
            const resp = await callAPI(`/api/transfer/create`, 'post', formData);
            setTransferInitiated(true);
            localStorage.setItem('pendingTransfer', '1');
            setIsLoading(false);
            setError(undefined);
            return Promise.resolve(resp);
        } catch (err) {
            localStorage.removeItem('pendingTransfer');
            setIsLoading(false);
            setError(err)
            return Promise.reject(err);
        }
    };

    const finalizeTransfer = async (evt: any) => {
        evt.preventDefault();
        if (!otpValue) {
            alert('Please enter OTP');
            return;
        }
        setIsLoading(true);

        try {
            await callAPI(`/api/transfer/confirm`, 'post', {otp: otpValue});
            setError(undefined);
            setIsLoading(false);
            setTransferCompleted(true);
            await dispatch(actions.fetchInitialData());
            localStorage.removeItem('pendingTransfer');
        } catch (err) {
            setError(err);
            setIsLoading(false);
            setTransferCompleted(false);
        }

    }

    const firstAccId = accounts[0].id;
    const userAccountsList = accounts.map(item => ({
        ...item,
        sortcode: item.sort_code,
        currency: item.currency_name
    }));

    useEffect(() => {
        const transferInProgress = localStorage.getItem('pendingTransfer');
        if (!!transferInProgress)
            setTransferInitiated(true);
    }, []);

    return (
        <div className="row">
            <div className="col-sm-8 offset-sm-2 col-md-6 offset-md-3">
                {isLoading && (
                    <div className="d-flex flex-column justify-content-center align-items-center my-3">
                        <Loader />
                    </div>
                )}
                <section className="module new-transfer">
                    {!transferCompleted && (
                        <>
                            {!transferInitiated && (
                                <>
                                    <h1>New transfer</h1>
                                    {error && <p>{error?.message}</p>}
                                    {/*@ts-ignore*/}
                                    <Form
                                        userId={userId}
                                        userAccountsList={userAccountsList}
                                        firstAccId={firstAccId}
                                        addTransfer={addTransfer}
                                    />
                                </>
                            )}

                            {transferInitiated && (
                                <form noValidate onSubmit={finalizeTransfer}>
                                    <h1>Enter OTP to complete transfer</h1>
                                    <div className="form-group">
                                        <label htmlFor="otp">Enter OTP</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="otp"
                                            name="otp"
                                            placeholder="Enter one time password..."
                                            onChange={e => setOtpValue(e.target.value)}
                                        />
                                    </div>
                                    <SingleModuleButton text="Confirm transfer" type="submit"/>
                                </form>
                            )}
                        </>
                    )}

                    {transferCompleted && (
                        <h3>Transfer completed successfully.</h3>
                    )}

                </section>
            </div>
        </div>
    );
};

export default NewTransfer;
