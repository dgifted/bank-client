import * as Yup from 'yup';
import 'tools/validations/YupCustomValidations';

// New transfer validations
export default Yup.object().shape({
   sourceAccountId: Yup.string().required('Please select the account'),

   payeeAccountNumber: Yup.number()
      .required('Please enter the account number')
      .typeError('Account must be a number')
      .positive('Account must be a positive number')
      .integer('Account must be an integer'),
      // .length(8, 'Account must be 8 numbers'),
      // .min(8, 'Account must be 8 numbers')
      // .max(8, 'Account must be 8 numbers'),

   payeeSortcode: Yup.string()
      .required('Please enter the sort code'),
      // .typeError('Sort code must be a number'),
      // .positive('Sort code must be a positive number')
      // .integer('Sort code must be an integer'),
      // .length(6, 'Sort code must be 6 numbers'),
      // .min(6, 'Sort code must be 6 numbers')
      // .max(6, 'Sort code must be 6 numbers'),

   payeeName: Yup.string()
      .required('Please enter the name'),
      // .min(2, 'Name must be at least 2 characters')
      // .max(50, 'Name must be a maximum of 50 characters'),

   payeeBankName: Yup.string()
      .required('Please enter receiver bank name'),

   amount: Yup.number()
      .required('Please enter the amount')
      // .typeError('Amount must be a number')
      // .positive('Amount must be a positive number')
});
