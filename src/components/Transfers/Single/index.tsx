import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { useAppSelector } from '@hooks';

import { formatDate } from 'tools';

interface Params {
   transId: string;
}

interface Props extends RouteComponentProps<Params> {}

const SingleTransfer: React.FC<Props> = (props) => {
   const transId = props.match.params.transId;
   const singleTrans = useAppSelector((state) =>
      state.transfers.data.find((el) => +el.id === +transId)
   );
   const transfer = {
       ...singleTrans,
      type: singleTrans.type_name,
      payeeName: singleTrans.payee_name,
      status: singleTrans.status_name,
      date: singleTrans.created_at,
      currency: singleTrans.currency.abbreviation
   };

   const { type, payeeName, amount, status, currency } = transfer;
   const date = formatDate(transfer.date, 'dd/MM/yyyy HH:mm');

   return (
      <section className="module single-transfer">
         <h1 className={'text-capitalize'}>{type}</h1>
         <ul>
            <li>Date: {date}</li>
            <li>Payee: {payeeName}</li>
            <li>Amount: <span className="text-uppercase">{currency}</span> {amount}</li>
            <li>Type: {type}</li>
            <li>Status: {status}</li>
         </ul>
      </section>
   );
};

export default SingleTransfer;
