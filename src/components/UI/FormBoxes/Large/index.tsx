import React from "react";
import loginIcon from "../login-icon.png";

const LargeFormBox: React.FC = (props) => (
  <section className="module large-form-module">
    <section className="login-icon">
      <div className="icon-container">
        <img src={loginIcon} className="img-responsive" alt="Login icon" />
      </div>
    </section>

    {props.children}
  </section>
);

export default LargeFormBox;
