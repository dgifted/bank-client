import React from 'react';

import loginIcon from '../login-icon.png';
import './style.scss';

type Props = {
    hideLogo?: boolean;
    className?: string;
};

const SmallFormBox: React.FC<Props> = (props) => {

    const iconClassName = props.className ? `login-icon ${props.className}` : 'login-icon';

    return (
        <section className="module small-form-module">
            {!props.hideLogo && (
                // <section className="login-icon">
                <section className={iconClassName}>
                    <div className="icon-container">
                        <img src={loginIcon} className="img-responsive" alt="Login icon"/>
                    </div>
                </section>
            )}


            {props.children}
        </section>
    )

};

export default SmallFormBox;
