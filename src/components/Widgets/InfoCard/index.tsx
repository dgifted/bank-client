import React from 'react';
// import classes from './InfoCard.module.css';

type Prop = {
    classes: string;
}

const InfoCard: React.FC<Prop> = (props) => {

    return(
        // <div className={`${classes.container} ${props.classes}`}>
        <div className={`${props.classes}`} style={{borderRadius: '10px', padding: '1rem'}}>
            {props.children}
        </div>
    );
}

export default InfoCard;
