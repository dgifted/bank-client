import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useAppDispatch } from "@hooks";
import * as H from "history";

import * as actions from "actions";
import LoginBox from "components/Auth/Login";
import { getAuthToken } from "tools";

type Props = {
  history: H.History;
};

const Login: React.FC<Props> = (props) => {
  const dispatch = useAppDispatch();
  const routerHistory = useHistory();

  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState("");

  const onLoginSubmit = (identifier: string, password: string) => {
    setIsLoading(true);
    setError("");

    const loginData = {
      identifier,
      password,
    };

    dispatch(actions.login(loginData))
      .then(() => routerHistory.replace('/panel'))
      .catch(console.log)
      .finally(() => setIsLoading(false));
  };

  useEffect(() => {
    const token = getAuthToken();
    if (!token) {
      routerHistory.replace('/login');
    }
  }, []);

  return (
    <div className="row">
      <div className="col">
        <LoginBox
          history={props.history}
          onLoginSubmit={onLoginSubmit}
          loading={isLoading}
          error={error}
        />
      </div>
    </div>
  );
};

export default Login;
