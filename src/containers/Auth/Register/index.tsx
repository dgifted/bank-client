import React, { useState } from "react";
// import {useHistory} from 'react-router-dom'
import { useAppDispatch } from "@hooks";
import * as H from "history";

import * as actions from "actions";
import RegisterBox from "components/Auth/Register";
import SingleModuleButton from "components/UI/Buttons/SingleModuleButton";
import { callAPI } from "api/base";

type Props = {
  history: H.History;
};

const Register: React.FC<Props> = (props) => {
  // const dispatch = useAppDispatch();
  // const routerHistory = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [registered, setRegistered] = useState(false);
  const [error, setError] = useState("");

  const onRegisterSubmit = (
    email: string,
    password: string,
    passwordConfirm: string,
    firstName: string,
    lastName: string,
    phone: string,
    dob: any,
    streetAddress: string,
    postCode: string,
    city: string,
    sex: string,
    country: string,
    occupation: string,
    accountType: string,
    currency: string,
    passport?: File | undefined
  ) => {
    setIsLoading(true);
    setError("");

    const accountTypeId = accountType.split(':')[0];
    const currencyId = currency.split(':')[0];

    const userData = {
      email,
      password,
      passwordConfirm,
      firstName,
      lastName,
      phone,
      dob,
      streetAddress,
      postCode,
      city,
      sex,
      country,
      occupation,
      currency: currencyId,
      accountType: accountTypeId,
      passport,
    };


    callAPI(`/api/auth/register`, "post", userData)
      .then(() => setRegistered(true))
      .catch((err) => {
        console.log(err)
        setError(err.message);
      })
      .finally(() => setIsLoading(false));

    //  dispatch(actions.register(userData))
    //    .then(() => {
    //      setRegistered(true);
    //    })
    //    .catch(console.log)
    //    .finally(() => setIsLoading(false));
  };

  return (
    <div className="row">
      <div className="col">
        {!registered && (
          <RegisterBox
            history={props.history}
            onRegisterSubmit={onRegisterSubmit}
            loading={isLoading}
            error={error}
          />
        )}
        {registered && (
          <div style={{ maxWidth: "700px", margin: '0 auto' }} className="d-flex flex-column align-items-center pt-5">
            <h3 className="mb-4">
              Your account has been created successfully.
            </h3>
            <p>A confirmatory email has been sent to your email address.</p>
            <div style={{ maxWidth: "300px" }}>
              <SingleModuleButton text="Login to account" href="/login"/>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Register;
