import React from "react";
import { Field, Form, withFormik, FormikProps } from "formik";
import * as Yup from "yup";
import SingleModuleButton from "components/UI/Buttons/SingleModuleButton";

// import * as H from "history";

// Shape of form values
interface FormValues {
    amount: string;
    method: string
}

const InnerForm = (props: FormikProps<FormValues>) => {
    const { errors, touched } = props;

    return (
        <Form className="pt-4">
            <div>
                <div className="form-group">
                    <Field
                        type="number"
                        className="form-control"
                        name="amount"
                        placeholder="Enter amount"
                    />
                    {touched.amount && errors.amount && (
                        <p className="field-invalid">{errors.amount}</p>
                    )}
                </div>
                <div className="form-group">
                    <Field as="select" className="form-control " name="method">
                        <option>Choose payment method</option>
                        <option value="fiat">Pay into account</option>
                        <option value="crypto">Pay using BTC</option>
                    </Field>
                    {touched.method && errors.method && (
                        <p className="field-invalid">{errors.method}</p>
                    )}
                </div>

            </div>

            <SingleModuleButton text="Proceed" type="submit" />
        </Form>
    );
};

// The type of props MyForm receives
interface MyFormProps extends FormValues {
    // history: H.History;
    onDeposit: (amount: string, method: string ) => void;
}

// Wrap our form with the using withFormik HoC
const DepositForm = withFormik<MyFormProps, FormValues>({
    // Transform outer props into form values
    mapPropsToValues: (props) => ({amount: "", method: ""}),

    // Add a custom validation function (this can be async too!)
    validationSchema: Yup.object().shape({
        amount: Yup.string()
            .required("Amount is required"),
        method: Yup.string()
            .required("Method is required")
    }),

    // Submission handler
    handleSubmit: (values, { props }) => {
        props.onDeposit(values.amount, values.method);
    },
})(InnerForm);

export default DepositForm;
