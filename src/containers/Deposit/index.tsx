import React, {useState} from "react";

import {callAPI} from "api/base";
import SmallFormBox from "components/UI/FormBoxes/Small";
import Loader from "components/UI/Loader";
import DepositForm from "containers/Deposit/Form";

const Deposit: React.FC = () => {
    // const [paymentOptions, setPaymentOptions] = useState<any>();
    const [error, setError] = useState<any>();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [submitted, setSubmitted] = useState<boolean>(false);

    // const initializePaymentGateways = async () => {
    //     setIsLoading(true);
    //     try {
    //         const resp = await callAPI(`/api/site-settings`);
    //         setPaymentOptions({
    //             receivingAccount: resp.receiving_account,
    //             walletAddress: resp.crypto_address
    //         });
    //         setIsLoading(false);
    //     } catch (e) {
    //         console.log(e)
    //         // setError(e)
    //         setIsLoading(false);
    //     }
    // };

    // useEffect(() => {
    //     initializePaymentGateways().then()
    // }, []);
    //
    const onDeposit = async (amount: string, method: string) => {
        console.log('amount and method -> ', amount, method);
        setIsLoading(true);

        try {
            await callAPI(`/api/deposit/create`, 'post', {
                amount, method
            });
            setIsLoading(false);
            setSubmitted(true);
        } catch (e) {
            console.log('Deposit error -> ', e);
            setIsLoading(false);
            setError(e.message || 'An unexpected error occurred. Please try again.')
        }
    }

    return (
        <div className="row">
            <div className="col">
                {!submitted && (
                    <>
                        <h3 className={'text-center my-3'}>Please fill in the form and click proceed</h3>
                        <SmallFormBox hideLogo={true}>
                            {error && (<p className="text-center text-danger">{error}</p>)}
                            {isLoading && <Loader/>}
                            {/*@ts-ignore*/}
                            <DepositForm onDeposit={onDeposit}/>
                        </SmallFormBox>
                    </>
                )}

                {submitted && (
                    <h4>Transaction created and awaiting confirmation.</h4>
                )}
            </div>
        </div>
    );
};

export default Deposit;
