import React, {useEffect, useState} from 'react';
import {useAppSelector} from '@hooks';

import IncomeStats from 'components/Widgets/IncomeStats';
import InfoCard from "components/Widgets/InfoCard";
import {callAPI} from "api/base";


type Props = {};

const PanelHome: React.FC<Props> = (props) => {
    const [balance, setBalance] = useState<number>(0);
    const [currency, setCurrency] = useState<any>();
    const [accountStatus, setAccountStatus] = useState<any>(0);
    const user = useAppSelector((state) => state.profile.data);
    let bgColor;
    let statusName;

    const fetchUserAccounts = async () => {
        try {
            const resp = await callAPI(`/api/account/user-accounts`);
            let _balance = 0;
            let _currency = {};
            let _status = 0;
            resp.data.data.forEach((item: any, index: number) => {
                if (index == 0) {
                    _currency = item.currency;
                    _status = item.status;
                }
                _balance += +item.balance;
            });
            setAccountStatus(_status);
            setBalance(_balance);
            setCurrency(_currency);
        } catch (err) {
            setBalance(0);
        }
    };

    // const transformAccountStatus = () => {
    switch (Number(accountStatus)) {
        case 1:
            statusName = 'Active';
            bgColor = 'bg-success';
            break;
        case 2:
            statusName = 'Dormant';
            bgColor = 'bg-dark';
            break;
        case 3:
            statusName = 'On-hold';
            bgColor = 'bg-warning';
            break;
        case 4:
            statusName = 'Suspended';
            bgColor = 'bg-danger';
            break;
        default:
            statusName = 'Inactive';
            bgColor = 'bg-primary';
            break;
    }
    // };

    useEffect(() => {
        fetchUserAccounts().then();
    }, []);

    // useEffect(() => {
    //     transformAccountStatus()
    // }, [transformAccountStatus]);

    return (
        <div className="row panel-content">
            <div className="col-md-12">
                <h1>
                    Welcome {user.first_name} {user.last_name}
                </h1>
            </div>

            <div className="col-md-8">
                <IncomeStats/>
            </div>
            <div className="col-md-4">
                <div className="row">
                    <div className="col-sm-6 col-md-12">
                        <InfoCard classes={'bg-primary text-white'}>
                            <h3>Balance</h3>
                            <p><i className="text-uppercase">{currency?.abbreviation}</i> {balance}</p>
                        </InfoCard>
                    </div>
                    <div className="col-sm-6 col-md-12 mt-md-3">
                        <InfoCard classes={`${bgColor} text-white`}>
                            <h3>Account status</h3>
                           <p><i className="text-uppercase">{statusName}</i></p>
                        </InfoCard>
                    </div>
                    {/*<div className="col-sm-6 col-md-12">*/}
                    {/*   <SingleMessage {...latestMessage} />*/}
                    {/*</div>*/}
                </div>
            </div>
        </div>
    );
};

export default PanelHome;
