import React, { useEffect } from 'react';
import { Route, Switch, useHistory } from 'react-router-dom';
import * as actions from 'actions';

import { useAppSelector, useAppDispatch } from '@hooks';

import { getAuthToken, isValidToken } from 'tools';

import Loader from 'components/UI/Loader';

import Navigation from 'containers/Navigation';
import PanelIntro from 'containers/Panel/Intro';
import PageNotFound from 'components/PageNotFound';

import Accounts from 'containers/Accounts';
import Transfers from 'containers/Transfers';
import Cards from 'containers/Cards';
import Profile from 'containers/Profile';
import ProfileChangeDetails from 'containers/Profile/ChangeDetails';
import Messages from 'containers/Messages';
import Help from 'components/Help';
import SubHeaderNav from "components/Navigation/SubHeader";
import Deposit from "containers/Deposit";
import ProfilePictureUpload from "containers/Profile/UploadPhoto";

type Props = {};

// Get all user's initial data or redirect back to /login if not logged in
// This is all handled in withAuth HOC
const Panel: React.FC<Props> = (props) => {
   const dispatch = useAppDispatch();
   const routerHistory = useHistory();

   const initialDataStatus = useAppSelector((state) => state.panel.initialDataStatus);

   const setAuthStatus = (status: boolean) => dispatch(actions.setAuthStatus(status));
   const fetchInitialData = () => dispatch(actions.fetchInitialData());

   const checkValidToken = async () => {
      try {
         await isValidToken();

         setAuthStatus(true);
         await fetchInitialData();
      } catch (err) {
         console.log('Token error in panel component');
         setAuthStatus(false);
         routerHistory.replace('/login');
      }
   };

   useEffect(() => {
      checkValidToken().then(r => r);
   }, []);

   if (!initialDataStatus) {
      return <Loader />;
   }

   return (
      <>
         <Route path="/panel" component={Navigation} />
         <SubHeaderNav/>

         <Switch>
            <Route exact path="/panel" component={PanelIntro} />
            <Route path="/panel/accounts" component={Accounts} />
            <Route path="/panel/transfers" component={Transfers} />
            <Route path="/panel/cards" component={Cards} />
            <Route path="/panel/profile" component={Profile} />
            <Route path="/panel/change-details" component={ProfileChangeDetails} />
            <Route path="/panel/messages" component={Messages} />
            <Route path="/panel/help" component={Help} />
            <Route path="/panel/deposit" component={Deposit} />
            <Route path="/panel/upload-profile-picture" component={ProfilePictureUpload} />
            <Route component={PageNotFound} />
         </Switch>
      </>
   );
};

export default Panel;
