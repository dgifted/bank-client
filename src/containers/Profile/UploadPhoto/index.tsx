import React, {useRef, useState} from 'react';
import {useSWRConfig} from 'swr';

import SmallFormBox from "components/UI/FormBoxes/Small";
import useAuth from "../../../@hooks/use-auth";
import Loader from "components/UI/Loader";
import SingleModuleButton from "components/UI/Buttons/SingleModuleButton";
import {callAPI} from "api/base";
import ProgressBar from "react-bootstrap/ProgressBar";

type Props = {};

const ProfilePictureUpload: React.FC<Props> = (props) => {
    const {data: profileData, isLoading} = useAuth();
    const [uploading, setUploading] = useState(false);
    const [uploadError, setUploadError] = useState(false);
    const [uploadedPercentage, setUploadedPercentage] = useState(0);
    const inputRef = useRef<any>();
    const {mutate} = useSWRConfig();

    const uploadProfilePhoto = async (evt: any) => {
        const {target} = evt;
        const formData = new FormData();
        formData.append('photo', target.files[0]);
        setUploading(true);
        try {
            await callAPI(`/api/upload-photo`, 'POST', formData, onUploadProgressHandler);
            await mutate('/api/auth/user');
            setTimeout(() => setUploading(false), 1000);
        } catch (e) {
            setUploadError(e.message);
            setUploading(false);
        }
    };

    const onUploadProgressHandler = (progressEvent: any) => {
        const {loaded, total} = progressEvent;
        const percentageUploaded = Math.floor((loaded * 100) / (total));
        console.log('Uploaded % -> ', percentageUploaded);

        if (percentageUploaded <= 100)
            setUploadedPercentage(percentageUploaded);
    }

    return (
        <div className="row panel-content">
            <div className="col">
                {isLoading ? <Loader/> : (
                    <SmallFormBox hideLogo={true}>
                        <div className={'pt-4'}>
                            <h4 className={'text-center'}>Change/Upload your profile picture</h4>

                            <div className={'d-flex flex-column justify-content-center align-items-center p-5'}>
                                <img
                                    alt={'Profile picture'}
                                    className={'rounded-circle mx-auto d-block'}
                                    src={profileData?.avatar}
                                    style={{
                                        width: '100%'
                                    }}
                                />
                            </div>
                            {uploading
                                ?
                                <div className={'my-2'}>
                                    <ProgressBar animated now={uploadedPercentage} label={`${uploadedPercentage}%`}/>
                                </div>
                                : null
                            }
                            <input
                                type={'file'}
                                accept={'image/*'}
                                className={'d-none'}
                                ref={inputRef}
                                onChange={uploadProfilePhoto}
                            />
                            <SingleModuleButton text="Change photo" type="submit" onClick={() => {
                                inputRef.current.click();
                            }}/>
                        </div>
                    </SmallFormBox>
                )}

            </div>
        </div>
    );
}

export default ProfilePictureUpload;
