import React, {useLayoutEffect, useState} from 'react';

import {useAppSelector} from '@hooks';
import './style.scss';
import ProfileHeader from 'components/Profile/Header';
import ProfileStats from 'components/Profile/Stats';
import ProfileLinks from 'components/Profile/Links';
import {callAPI} from "api/base";

type Props = {};

const Profile: React.FC<Props> = (props) => {
    const profile = useAppSelector((state) => state.profile.data);
    const [userProfile, setUserProfile] = useState<any>();
    profile.stats = {
        accsDetails: {
            count: 0,
            balance: 0,
        },
        messagesCount: 0
    };

    const links = [
        // {href: '/panel/transfers', text: 'Transfers', icon: 'ion-card'},
        // {href: '/panel/upload-profile-picture', text: 'Upload Picture', icon: 'ion-card'},
        {href: '/panel/upload-profile-picture', text: 'Upload Picture', icon: 'ion-camera'},
        {
            href: '/panel/change-details',
            text: 'Change details',
            icon: 'ion-android-checkbox-outline',
        },
    ];

    const fetchUserProfile = async () => {
        try {
            const resp = await callAPI(`/api/user-profile`);
            const fetchedData = resp['0'];
            const profileData = {
                createdAt: fetchedData.created_at,
                firstName: fetchedData.first_name,
                lastName: fetchedData.last_name,
                streetAddr: fetchedData.street_address,
                postcode: fetchedData.post_code,
                city: fetchedData.city,
                email: fetchedData.email,
                avatar: fetchedData.avatar,
                stats: {
                    accsDetails: resp.stats.accsDetails,
                    messagesCount: resp.stats.messagesCount,
                }

            };
            setUserProfile(profileData);
        } catch (err) {
            console.log('Error fetching user profile -> ', err);
        }
    };

    useLayoutEffect(() => {
        fetchUserProfile().then();
    }, []);

    return (
        <div className="row panel-content">
            <div className="col">
                <section className="module profile">
                    <ProfileHeader profile={userProfile}/>
                    <ProfileStats stats={userProfile?.stats}/>
                    <ProfileLinks links={links}/>
                </section>
            </div>
        </div>
    );
};

export default Profile;
