import ReactDOM from 'react-dom';
import {SWRConfig} from 'swr';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';

import App from 'components/App';
// Store
import {history, store} from 'store';
import {ConnectedRouter} from 'connected-react-router';
import React from "react";
import {callAPI} from "api/base";

// Render
ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <ConnectedRouter history={history}>
                <SWRConfig value={{fetcher: (url) => callAPI(url)}}>
                    <App/>
                </SWRConfig>
            </ConnectedRouter>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
