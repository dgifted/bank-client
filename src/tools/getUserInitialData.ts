import { getMyself } from 'api/users';
import { getMyAccounts } from 'api/accounts';
import { getMyCards } from 'api/cards';
import { getMyTransfers } from 'api/transfers';
import { getMyMessages } from 'api/messages';
// import {getMyNotifications} from "api/notifications";
// import {getMyN}

// Fetch data for all the sections
const getUserInitialData = async () => {
   try {
      const data = {
         user: {},
         accounts: [],
         cards: [],
         transfers: [],
         messages: [],
         // notifications: [],
      };

      const userDetails = await getMyself();
      const userAccounts = await getMyAccounts();
      const userCards = await getMyCards();
      const userTransfers = await getMyTransfers();
      const userMessages = await getMyMessages();
      // const userNotifications = await getMyNotifications()

      data.user = userDetails.data;
      data.accounts = userAccounts.data.data;
      data.cards = userCards.data.data;
      data.transfers = userTransfers.data.data;
      data.messages = userMessages.data.data;
      // data.notifications = userNotifications.data.data

      return data;
   } catch (err) {
      throw new Error(err as unknown as string);
   }
};

export default getUserInitialData;
