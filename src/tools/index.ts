import {updateAPIConfig} from 'api/base';
import {callAPI} from 'api/base';
import format from 'date-fns/format';

// Add padding from the start of the current string/number
export const myPadStart = (el: string | number, targetLength: number, padString: string) => {
    return el.toString().padStart(targetLength, padString);
};

// Insert something after every n characters in the string
export const chunker = (el: [] | string | number, step: number, string: string) => {
    const regExp = new RegExp(`.{${step}}`, 'g');

    return el?.toString()?.match(regExp)?.join(string) || '';
};

// Format date
export const formatDate = (date: Date, dateFormat: string) => format(new Date(date), dateFormat);

// Token checker
export const getAuthToken = () => {
    return localStorage.getItem('token');
};

export const setAuthToken = (token: string) => {
    localStorage.setItem('token', token);
};

export const removeAuthToken = () => {
    localStorage.removeItem('token');
};

const checkToken = async (token: string) => {
    if (!token) return false;

    updateAPIConfig({
        authToken: token
    });

    let loggedIn: boolean;

    try {
        await callAPI(`/api/auth/status`);
        loggedIn = true;
    } catch (err) {
        loggedIn = false;
    }

    return loggedIn;
};

export const isValidToken = () => {
    return new Promise(async (resolve, reject) => {
        // Check if token is present
        const token: string | null = getAuthToken();

        if (!token) {
            reject();
            return;
        }

        const tokenValid = await checkToken(token);

        // If token expired - remove it
        if (!tokenValid) {
            removeAuthToken();

            reject();
            return;
        }

        resolve(token);
    });
};

export const getRandomString = (length: number) => {
   let result = '';
   const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   const charactersLength = characters.length;
   for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}
